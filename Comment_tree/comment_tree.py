from typing import List
import sys, json
from pathlib import Path

class Comment:
    id: str = ""
    content: str = ""
    replies = []
    def __init__(self, id, content, replies):
        self.id = id
        self.content = content
        self.replies = replies
    def __eq__(self, other):
        return self.id == other.id and self.content == other.content and self.replies == other.replies


def f(jsn : dict, comment: Comment) -> List[Comment]:
    fname = Path(path / (str(jsn['id']) + '.txt'))    
    if not fname.exists():
        print(f"File {str(jsn['id'])}.txt doesn't exist!")
        exit(1)
        
    with open(fname, 'r') as cf:
        res = Comment(
            str(jsn['id']),
            cf.read(),
            [f(c, Comment('', '', [])) for c in jsn['replies']]
        )
    return res

def pr(c:Comment) -> dict:
    return {
        'id': c.id,
        'content': c.content,
        'replies': [pr(r) for r in c.replies]
    }

def test_parse():
    correctComment: Comment = Comment(
        '1', 'lorem ipsum\n', [
            Comment('2', 'dolor sit amet\n', []),
            Comment('3', 'consectetur adipiscing elit\n', [
                Comment('4', 'sed do eiusmod\n', []),
                Comment('5', 'tempor incididunt ut labore\n', [])
                ])
            ]
    )
    assert c == correctComment


def test_format():
    assert res == {
        "id": "1",
        "content": "lorem ipsum\n",
        "replies": [
            {
                "id": "2",
                "content": "dolor sit amet\n",
                "replies": []
            },
            {
                "id": "3",
                "content": "consectetur adipiscing elit\n",
                "replies": [
                    {
                        "id": "4",
                        "content": "sed do eiusmod\n",
                        "replies": []
                    },
                    {
                        "id": "5",
                        "content": "tempor incididunt ut labore\n",
                        "replies": []
                    }
                ]
            }
        ]
    }


if len(sys.argv) < 2:
    sys.exit("Not enough arguments!")

path = Path(sys.argv[1])
if not path.exists():
    sys.exit("Directory doesn't exist!")

data = json.load(sys.stdin)
c = f(data, Comment('', '', []))

test_parse() 

res = pr(c)
test_format()

print(json.dumps(res, indent=2))