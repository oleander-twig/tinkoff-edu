""" Built in unittest module"""
import unittest
from cargo_transportation import Logistics

class Tests(unittest.TestCase):
    """ class to tests Game class"""

    def setUp(self) -> None:
        self.log = Logistics()

    def test_first(self):
        """ First test """
        self.assertEqual(5, self.log.count_time("A"), "Wrong time path")

    def test_second(self):
        """ Second test """
        self.assertEqual(5, self.log.count_time("B"), "Wrong time path")

    def test_third(self):
        """ Third test """
        self.assertEqual(5, self.log.count_time("AB"), "Wrong time path")

    def test_fourth(self):
        """ Fourth test """
        self.assertEqual(5, self.log.count_time("BA"), "Wrong time path")

    def test_fifth(self):
        """ Fifth test """
        self.assertEqual(7, self.log.count_time("BAA"), "Wrong time path")


unittest.main()
