""" Module """
class Logistics():
    """ Calculate the time of logistic wide """
    def __init__(self, time=0) -> None:
        self.time = time

    def time_loc(self) -> int:
        """ Return time """
        return self.time

    def count_time(self, path) -> int:
        """ Calculate time """
        positions = ['S', 'S', 'P']
        roads = [0, 0, 0]
        cargo_in_p = 0
        while True:
            for i in range(2):
                if roads[i] == 0 and positions[i] == 'S-B':
                    cargo_in_p += 1

            if not (any(roads) or path or cargo_in_p):
                break

            for i in range(3):
                if roads[i] == 0:
                    positions[i] = positions[i][-1]

            if path and positions[0] == 'S' and roads[0] == 0:
                roads[0] = 1 if path[0] == 'B' else 5
                positions[0] = 'S-B' if path[0] == 'B' else 'S-A'
                path = path[1::]

            if path and roads[0] == 0 and positions[0] != 'S':
                roads[0] = 1 if positions[0] == 'B' else 5
                positions[0] = 'B-S' if positions[0] == 'B' else 'A-S'

            if path and positions[1] == 'S' and roads[1] == 0:
                roads[1] = 1 if path[0] == 'B' else 5
                positions[1] = 'S-B' if path[0] == 'B' else 'S-A'
                path = path[1::]

            if path and roads[1] == 0 and positions[1] != 'S':
                roads[1] = 1 if positions[0] == 'B' else 5
                positions[1] = 'B-S' if positions[1] == 'B' else 'A-S'

            if cargo_in_p != 0 and roads[2] == 0 and positions[2] == 'P':
                roads[2] = 4
                positions[2] = 'P-B'
                cargo_in_p -= 1

            if positions[2] != 'P' and \
                (cargo_in_p != 0 or 'B' in path or positions[0] == 'S-B' or positions[1] == 'S-B'):
                roads[2] = 4
                positions[2] = 'B-P'

            delta = min([i for i in roads if i])
            roads = [max(d - delta, 0) for d in roads]
            self.time += delta
        return self.time
    