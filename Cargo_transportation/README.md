# Cargo transportation App

#### Выполнено по коду [Артёма Балабанова](https://gitlab.com/artyomVB/cargo/).

## Описание приложения 

Приложение вычисляет минимальное общее количество времени затраченное на доставку всех контейнеров. Оно также предоставляет возможность пользователю в интерактивном режиме моделировать перемещение грузов, а именно - возможности поставить груз на склад в произвольный момент времени.

## Пример использования 

Приложение взаимодействует с данными формата JSON. 

Чтобы добавить контейнер, который нужно доставить из пункта 'S' в пункт 'B', необходимо выполнить следующую команду в консоли:
```
{"command": "cargo.supply", "route": {"source": "S", "destination": "B"}}
```
Чтобы продвинуть время вперед до момента, в котором произойдет следующее событие, выполните команду:
```
{"command": "time.advance"}
```

## Приложение можно скачать [тут](https://gitlab.com/oleander-twig/tinkoff-edu/-/blob/hw-11/CargoDispatcher)

## FAQ
- [Как расшифровать ответ?](#Как-расшифровать-ответ)
- [Как поставить груз, который поедет в пункт 'A'?](#Как-поставить-груз-который-поедет-в-пункт-A)

### Как расшифровать ответ?
Пусть программа в качестве ответа вывела следующую строку:
```
{"event": "transport.arrived", "time": 5, "transport": {"id": 2, "type": "truck"}, "location": "A", "cargoes": [{"id": 1, "route": {"source": "S", "destination": "A"}}]}
```
Как её интерпритировать? Пойдём по порядку. Что произошло? - Транспорт приехал. Сколько времени прошло с начала? - `5` единиц измерения времени. Какой транспорт доехал до места назначения? - Грузовик под номером `2`. Куда приехал? - В точку `А`. Какой груз контейнер доставил? - Контейнер под номером `2`, который должен был быть доставлен в пункт `А`.

### Как поставить груз, который поедет в пункт 'A'?
Для этого необходимо выполнить следующую команду:
```
{"command": "cargo.supply", "route": {"source": "S", "destination": "A"}}
```

## За помощью писать [сюда](https://t.me/oleander_twig)
## Инструкция по запуску программы
После скачивания файла необходимо перейти в директорию, куда был сохранен файл и с помощью команды в терминале, приведеной ниже, начать работу с программой.
```
./CargoDispatcher
```
Для завершения работы необходимо сочетание клавиш `Control + C`

## Лицензия
Copyright © 2021 <Анастасия Северенкова>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.