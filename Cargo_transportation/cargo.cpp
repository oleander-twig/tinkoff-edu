#include <deque>
#include <string>
#include <vector>
#include <iostream>

class Node {
public:
    int id;
    std::string name;

    Node(int id_, std::string name_)  : id(id_), name(name_) {} 
};

class Graph {
public:
    std::vector<Node> nodes {Node(0, "S"), Node(1, "P"), Node(2, "A"), Node(3, "B")};
    std::vector<std::vector<int>> edges {{-1, 1, 5, -1}, 
                                         {1, -1, -1, 4}, 
                                         {5, -1, -1, -1}, 
                                         {-1, 4, -1, -1}};
};


class Path {
public:
    int source_node_id;
    int destination_node_id;
    std::vector<int> node_ids;

    Path(int s, int d, std::vector<int> vec) : source_node_id(s), destination_node_id(d), node_ids(vec) {} 
    
    void operator=(Path other) {
        this->source_node_id = other.source_node_id;
        this->destination_node_id = other.destination_node_id;
        this->node_ids = other.node_ids;
    }
};

class Item {
public:
    int id;
    int source;
    int destination;
    Path path;
    int current_idx;

    Item() : path(-1, -1, {}) {
        id = -1;
        source = -1;
        destination = -1;
        current_idx = -1;
    }

    Item(int id_, Path path_) : id(id_), path(path_) {
        source = path.source_node_id;
        destination = path.destination_node_id;
        current_idx = 0;
    }

    void operator=(Item other) {
        this->id = other.id;
        this->source = other.source;
        this->destination = other.destination;
        this->path = other.path;
        this->current_idx = other.current_idx;
    }
};

class Planner {
public:
    Graph& g;

    Planner(Graph& g_) : g(g_) {}

    Path get_path(int d) {
        if (d == 'B') {
            std::vector<int> n{0, 1, 3};
            return Path(0, 3, n);
        } 
        std::vector<int> n{0, 2};
        return Path(0, 2, n);
    }
};

class Vehicle {
public: 
    int id;
    int start;
    int stop;
    int base_node;
    bool in_work;
    bool with_cargo;
    Item item;
    Graph& g;
    int time_remained_to_next_node;
    std::string type;

    Vehicle(int id_, int base_node_, Graph& g_, std::string type_) : id(id_), base_node(base_node_), g(g_), type(type_) {
        in_work = false;
        with_cargo = false;
    }

    int increment_time() {
        if (!in_work) {
            return 0;
        }
        time_remained_to_next_node--;
        if (time_remained_to_next_node == 0) {
            return 1; 
        }
        return 0;
    }

    void start_route(int start_node_id, int stop_node_id) {
        in_work = true;
        start = start_node_id;
        stop = stop_node_id;
        time_remained_to_next_node = g.edges[start_node_id][stop_node_id];
        item.current_idx++;
    }

    void put_cargo(Item item) {
        with_cargo = true;
        this->item = item;
    }

    Item get_cargo() {
        return item;
    }

    void reset_cargo() {
        with_cargo = false;
        in_work = false;
    }
};

class Event {
public:
    std::string type;
    Vehicle* vehicle;
    Item* item;
    int ticks;
};

class Query {
public:
    std::string type;
    std::string destination;
};

class JSONSerializer {
public:
    JSONSerializer() = default;

    Query deserialize(std::string& json) {
        if (json.find("time.advance") != std::string::npos) {
            return Query{"advance", ""};
        }
        if (json.find("A") == std::string::npos) {
            return Query{"supply", "B"};
        } 
        return Query{"supply", "A"};
    }

    std::string serialize(Event& event) { 
        std::string s = "{\"event\" : \"" + event.type + "\", \"time\" : " + std::to_string(event.ticks) + "\", ";
        if (event.vehicle != NULL) {
            s = s + "\"transport\" : {\"id\" : \"" + std::to_string(event.vehicle->id) + "\", type\": " + event.vehicle->type + "\" }";
        } 
        if (event.item != NULL) {
            s = s + "\"cargo\" : {\"id\" : \"" + std::to_string(event.item->id) + "\", \"route\" : {\"source\" : \"" + std::to_string(event.item->source) + "\", \"destination\" : \"" + std::to_string(event.item->destination) + "\"}}";
        }
        return s;
    }
};

class EventBus {
public:
    std::deque<Event> events;
    JSONSerializer json_serializer;

    void push(std::string type, Vehicle* vehicle, Item* item, int ticks) {
        events.push_front(Event{type, vehicle, item, ticks});
    }

    void flush() {
        while (!events.empty()) {
            std::cout << json_serializer.serialize(events.back()) << "\n";
            events.pop_back();
        }
    }
};

class DeliveryManager {
public:
    std::vector<int> destinations;
    std::vector<std::deque<Item>>& queues;

    DeliveryManager(std::string& s, std::vector<std::deque<Item>>& queues_) : queues(queues_) {
        for (int i = 0; i < s.size(); i += 2) {
            destinations.push_back(s[i]);    
        }
    }

    void init_queues(int id, Planner& planner) {
        for (int i = 0; i != destinations.size(); ++i) { 
            Item item{id, planner.get_path(destinations[i])};  
            queues[0].push_front(item);  
        }
    }
};



class QueryProccesor {
public:
    Planner& planner;
    std::vector<std::deque<Item>>& queues;
    EventBus& eb;
    int counter;

    QueryProccesor(Planner& p, std::vector<std::deque<Item>>& q, EventBus& eb_) : planner(p), queues(q), eb(eb_) {
        counter = 0;
    }

    int process_query(Query& query) {
        if (query.type == "supply") {
            DeliveryManager dm(query.destination, queues);
            dm.init_queues(counter, planner);
            ++counter;
            return 1;
        }
        return 0;
    }
};

class ConsoleApp {
public:
    JSONSerializer& js;
    QueryProccesor& qp;

    ConsoleApp(JSONSerializer& js_, QueryProccesor& qp_) : js(js_), qp(qp_) {}

    int wait_query() {
        std::string json;
        std::getline(std::cin, json);
        auto query = js.deserialize(json);
        return qp.process_query(query);
    }
};

class EventDispatcher {
public:
    std::vector<Vehicle>& vehicles;
    std::vector<std::deque<Item>>& queues;
    EventBus& eb;
    ConsoleApp& console_app;
    int remained_cargos;
    int ticks;
    
    EventDispatcher(std::vector<Vehicle>& vehicles_, std::vector<std::deque<Item>>& queues_, EventBus& eb_, ConsoleApp& ca) : vehicles(vehicles_), queues(queues_), remained_cargos(queues_[0].size()), eb(eb_), console_app(ca) {
        ticks = 0;
    }

    int process() {
        int flag2 = 0;
        int counter = 0;
        while (console_app.wait_query() == 1) {
            eb.push("supply", NULL, &queues[0].front(), ticks);
            eb.flush();
            flag2 = 1;
            counter++;
            remained_cargos = 0;
            for (int i = 0; i != queues.size(); ++i) {
                remained_cargos += queues[i].size();
            }
        }
        while (flag2 == 1) {
            flag2 = 0;
            for (int i = 0; i != counter; ++i) {
                on_cargo_supply(0);
            }
            eb.flush();
            while (console_app.wait_query() == 1) {
                eb.push("supply", NULL, &queues[0].front(), ticks);
                eb.flush();
                flag2 = 1;
                remained_cargos = 0;
                for (int i = 0; i != queues.size(); ++i) {
                    remained_cargos += queues[i].size();
                }
            }
        }
        while (remained_cargos != 0) {
            ++ticks;
            std::vector<int> arrived;
            int flag = 0;
            for (int i = 0; i != vehicles.size(); ++i) {
                if (vehicles[i].increment_time() == 0) {
                    continue;
                }
                flag = 1;
                arrived.push_back(i);
            }
            for (auto i : arrived) {
                on_arrival(vehicles[i], vehicles[i].stop);
            }
            if (flag == 1) {
                eb.flush();
                flag = 0;
                counter = 0;
                while (console_app.wait_query() == 1) {
                    eb.push("supply", NULL, &queues[0].front(), ticks);
                    eb.flush();
                    flag2 = 1;
                    counter++;
                    remained_cargos = 0;
                    for (int i = 0; i != queues.size(); ++i) {
                        remained_cargos += queues[i].size();
                    }
                }
                while (flag2 == 1) {
                    flag2 = 0;
                    for (int i = 0; i != counter; ++i) {
                        on_cargo_supply(0);
                    }
                    eb.flush();
                    while (console_app.wait_query() == 1) {
                        eb.push("supply", NULL, &queues[0].front(), ticks);
                        eb.flush();
                        flag2 = 1;
                        remained_cargos = 0;
                        for (int i = 0; i != queues.size(); ++i) {
                            remained_cargos += queues[i].size();
                        }
                    }
                }
            }
        }
        return ticks;
    }

    void on_arrival(Vehicle& vehicle, int node_id) {
        if (vehicle.with_cargo) {
            Item cargo(vehicle.get_cargo());
            vehicle.reset_cargo();
            if (cargo.destination != node_id) {
                queues[node_id].push_front(cargo);   
                eb.push("arrival", &vehicle, NULL, ticks);
                on_cargo_supply(node_id); 
            } else {
                eb.push("delivery", &vehicle, NULL, ticks);
                remained_cargos -= 1;
            }
            vehicle.start_route(vehicle.stop, vehicle.start);
            eb.push("departure", &vehicle, NULL, ticks);
        } else {
            eb.push("arrival", &vehicle, NULL, ticks);
            if (!queues[node_id].empty()) {
                Item cargo(queues[node_id].back());
                queues[node_id].pop_back();
                vehicle.put_cargo(cargo);
                vehicle.start_route(cargo.path.node_ids[cargo.current_idx], cargo.path.node_ids[cargo.current_idx + 1]);
                eb.push("departure", &vehicle, NULL, ticks); 
            } else {
                vehicle.in_work = false;
            }
        }
    }

    void on_cargo_supply(int node_id) {
        for (int i = 0; i != vehicles.size(); ++i) {
            if (!vehicles[i].in_work && vehicles[i].base_node == node_id) { 
                Item cargo = queues[node_id].back();
                queues[node_id].pop_back();
                vehicles[i].put_cargo(cargo);
                vehicles[i].start_route(cargo.path.node_ids[cargo.current_idx], cargo.path.node_ids[cargo.current_idx + 1]);
                eb.push("departure", &vehicles[i], NULL, ticks);
                break;
            }
        }
    }
};

int main() {
    Graph g;
    std::vector<Vehicle> vehicles {Vehicle(0, 0, g, "truck"), Vehicle(1, 0, g, "truck"), Vehicle(2, 1, g, "ship")};
    Planner planner(g);
    std::vector<std::deque<Item>> queues(4);
    JSONSerializer js;
    EventBus eb;
    QueryProccesor qp(planner, queues, eb);
    ConsoleApp console_app(js, qp);
    EventDispatcher dispatcher(vehicles, queues, eb, console_app);
    dispatcher.process();
}
