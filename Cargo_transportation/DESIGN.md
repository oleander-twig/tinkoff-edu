# Cargo transportation App

## Общее описание приложения 

Приложение вычисляет минимальное общее количество времени затраченное на доставку всех контейнеров. Оно также предоставляет возможность пользователю в интерактивном режиме моделировать перемещение грузов, а именно - возможности поставить груз на склад в произвольный момент времени.

## Пример использования 

Приложение взаимодействует с данными формата JSON. 

Чтобы добавить контейнер, который нужно доставить из пункта 'S' в пункт 'B', необходимо выполнить следующую команду в консоли:
```
{"command": "cargo.supply", "route": {"source": "S", "destination": "B"}}
```
Чтобы продвинуть время вперед до момента, в котором произойдет следующее событие, выполните команду:
```
{"command": "time.advance"}
```

## Файл для сборки находится [тут](https://gitlab.com/oleander-twig/tinkoff-edu/-/blob/hw-11/cargo.cpp)

## Приложение можно скачать [тут](https://gitlab.com/oleander-twig/tinkoff-edu/-/blob/hw-11/CargoDispatcher)

## FAQ
- [Файл не собирается? Вылезает сообщение о куче ошибок](#Файл-не-собирается-Вылезает-сообщение-о-куче-ошибок)
- [Как поставить груз, который поедет в пункт 'A'?](#Как-поставить-груз-который-поедет-в-пункт-A)

### Файл не собирается? Вылезает сообщение о куче ошибок
Такое случается, если дефолтный стандарт языка не совпадает со стандартом `c++`. В таком случае, лучше явно указать:
```
g++ cargo.cpp --std=c++17
```

### Как поставить груз, который поедет в пункт 'A'?
Для этого необходимо выполнить следующую команду:
```
{"command": "cargo.supply", "route": {"source": "S", "destination": "A"}}
```

## Инструкция по установке

Чтобы собрать файл с программой, необходимо выполнить следующую команду в директории, где лежит файл:
```
g++ cargo.cpp
```
Если никаких ошибок не вывелось, то можно запускать:
```
./a.out
```
Для завершения работы необходимо сочетание клавиш `Control + C`

## Для осуществления обратной связи писать [сюда](https://t.me/oleander_twig)

## Архитектура приложения

### Общие характеристики 

| Параметры                                  | Значения                 |
| ------------------------------------------ | :----------------------: |
| Алгоритмическая сложность                  | сложна                   |
| Требования к средствам обработки программы | Стандарт `c++17`         |
| Версия компилятора                         | clang-12.0.5,  gcc-11.2  |
| Поддерживаемые операционные системы        | Linux, Mac Os, Windows   |
| Размер бинарного файла                     | 213 КБ                   |
| Требуемая разрядность процессора           | x64                      |

### Иерархия классов 
![Class-diagram](https://gitlab.com/oleander-twig/tinkoff-edu/-/raw/hw-11/templates/Class-diagram.png)

### Иллюстрация структуры
![Work_into_cargo_diag](https://gitlab.com/oleander-twig/tinkoff-edu/-/raw/hw-11/templates/Work_into_cargo_diag.drawio.png)

## Лицензия 

Copyright © 2021 <Анастасия Северенкова>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.